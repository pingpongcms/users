@extends('dashboard::layouts.master')

@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                <h3>Users ({{ $users->total() }})</h3>
            </div>
            <div class="col-md-3">
                @include('dashboard::partials.search-form')
            </div>
            <div class="col-md-1">
                <a href="{!! route('admin.users.create') !!}" class="btn btn-default">Add New</a>
            </div>
        </div>
    </div>
    <table class="table table-stripped table-bordered">
        <thead>
            <th class="text-center">#</th>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Created At</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td class="text-center">{!! $no !!}</td>
                    <td>{!! $user->name !!}</td>
                    <td>{!! $user->username !!}</td>
                    <td>{!! $user->email !!}</td>
                    <td>{!! $user->created_at !!}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['admin.users.destroy', $user->id]]) !!}
                            <a href="{!! route('admin.users.show', $user->id) !!}" class="btn btn-sm btn-default" title="View" data-toggle="tooltip"><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{!! route('admin.users.edit', $user->id) !!}" class="btn btn-sm btn-default" title="Edit" data-toggle="tooltip"><i class="glyphicon glyphicon-edit"></i></a>
                            <button type="submit" class="btn btn-sm btn-default" title="Delete" data-toggle="tooltip"><i class="glyphicon glyphicon-trash"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
                <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">
        <div class="text-center">{!! $users !!}</div>
    </div>
</div>
@stop