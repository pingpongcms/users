@extends('dashboard::layouts.master')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Add New Permission
            <div class="panel-nav pull-right" style="margin-top: -7px;">
                <a href="{!! route('admin.permissions.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
        <div class="panel-body">
            @include('dashboard::permissions.form')
        </div>
    </div>

@stop