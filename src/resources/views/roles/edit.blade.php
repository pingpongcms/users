@extends('dashboard::layouts.master')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Role
            <div class="panel-nav pull-right" style="margin-top: -7px;">
                <a href="{!! route('admin.roles.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
        <div class="panel-body">
            @include('users::roles.form', ['model' => $role])
        </div>
    </div>

@stop