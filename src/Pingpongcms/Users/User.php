<?php

namespace Pingpongcms\Users;

use App\Post;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Pingpongcms\Support\Traits\Searchable;
use Pingpongcms\Users\Traits\HasRoles;

class User extends Authenticatable
{
    use Searchable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Hash password automatically.
     * 
     * @param string $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Relation has-many posts.
     * 
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
