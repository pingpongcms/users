<?php

namespace Pingpongcms\Users\Http\Requests\Users;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->segment(3),
            'username' => 'required|alpha_num|unique:users,username,'.$this->segment(3),
        ];

        if ($this->has('password')) {
            $rules['password'] = 'required|min:6';
        }

        return $rules;
    }
}
