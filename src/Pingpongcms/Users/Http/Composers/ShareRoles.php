<?php

namespace Pingpongcms\Users\Http\Composers;

use Pingpongcms\Users\Role;

class ShareRoles
{
    public function compose($view)
    {   
        $roles = Role::pluck('name', 'id')->toArray();
        
        $view->withRoles($roles);
    }
}