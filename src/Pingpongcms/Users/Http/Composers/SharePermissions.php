<?php

namespace Pingpongcms\Users\Http\Composers;

use Pingpongcms\Users\Permission;

class SharePermissions
{
    public function compose($view)
    {   
        $permissions = Permission::pluck('name', 'id')->toArray();
        
        $view->with(compact('permissions'));
    }
}