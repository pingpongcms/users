<?php

namespace Pingpongcms\Users\Http\Controllers;

use App\Http\Controllers\Controller;
use Pingpongcms\Users\Http\Requests\Roles\CreateRoleRequest;
use Pingpongcms\Users\Http\Requests\Roles\UpdateRoleRequest;
use Pingpongcms\Users\Role;

class RolesController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->authorize('index-role');

        $roles = Role::latest()->paginate(20);

        $no = $roles->firstItem();

        return view('users::roles.index', compact('roles', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('create-role');

        return view('users::roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
        $this->authorize('create-role');

        $role = Role::create($request->all());

        $role->permissions()->sync($request->get('permission_list', []));

        return redirect()->route('admin.roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('show-role');

        $role = Role::findOrFail($id);

        return view('users::roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('update-role');

        $role = Role::findOrFail($id);

        return view('users::roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $this->authorize('update-role');

        $role = Role::findOrFail($id);

        $role->update($request->all());

        $role->permissions()->sync($request->get('permission_list', []));

        return redirect()->route('admin.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('destroy-role');

        $role = Role::findOrFail($id);

        $role->delete();

        return redirect()->route('admin.roles.index');
    }
}
