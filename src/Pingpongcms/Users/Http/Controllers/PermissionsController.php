<?php

namespace Pingpongcms\Users\Http\Controllers;

use App\Http\Controllers\Controller;
use Pingpongcms\Users\Http\Requests\Permissions\CreatePermissionRequest;
use Pingpongcms\Users\Http\Requests\Permissions\UpdatePermissionRequest;
use Pingpongcms\Users\Permission;

class PermissionsController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->authorize('index-permission');

        $permissions = Permission::latest()->paginate(20);

        $no = $permissions->firstItem();

        return view('users::permissions.index', compact('permissions', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('create-permission');

        return view('users::permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreatePermissionRequest $request)
    {
        $this->authorize('create-permission');

        $permission = Permission::create($request->all());

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('show-permission');

        $permission = Permission::findOrFail($id);

        return view('users::permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('update-permission');

        $permission = Permission::findOrFail($id);

        return view('users::permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        $this->authorize('update-permission');

        $permission = Permission::findOrFail($id);

        $permission->update($request->all());

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('destroy-permission');

        $permission = Permission::findOrFail($id);

        $permission->delete();

        return redirect()->route('admin.permissions.index');
    }
}
