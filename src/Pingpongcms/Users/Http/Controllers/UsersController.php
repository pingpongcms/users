<?php

namespace Pingpongcms\Users\Http\Controllers;

use App\Http\Controllers\Controller;
use Pingpongcms\Users\Http\Requests\Users\CreateUserRequest;
use Pingpongcms\Users\Http\Requests\Users\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->authorize('index-user');

        $users = User::latest()->search($request->q)->paginate(20);

        $no = $users->firstItem();

        return view('users::admin.index', compact('users', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('show-user');

        return view('users::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->authorize('create-user');

        $user = User::create($request->all());

        $user->roles()->sync($request->role_list);

        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('show-user');

        $user = User::findOrFail($id);

        return view('users::admin.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('update-user');

        $user = User::findOrFail($id);

        return view('users::admin.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $this->authorize('update-user');

        $user = User::findOrFail($id);

        $user->update($request->all());

        $user->roles()->sync($request->role_list);

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('destroy-user');

        $user = User::findOrFail($id);

        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
