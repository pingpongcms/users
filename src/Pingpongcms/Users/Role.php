<?php

namespace Pingpongcms\Users;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    protected $appends = [
        'permission_list',
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function getPermissionListAttribute()
    {
        return $this->permissions->lists('id', 'name')->toArray();
    }

    public function addPermission($name)
    {
        return $this->permissions()->attach(
            Permission::whereIn('name', (array) $name)->lists('id')->toArray()
        );
    }

    public function removePermission($name)
    {
        return $this->permissions()->detach(
            Permission::whereIn('name', (array) $name)->lists('id')->toArray()
        );
    }

    public function syncPermission($name)
    {
        return $this->permissions()->sync(
            Permission::whereIn('name', (array) $name)->lists('id')->toArray()
        );
    }
}
