<?php

namespace Pingpongcms\Users\Repositories;

use Pingpongcms\Users\User;

class UserRepository
{
    public function find($author)
    {
        return User::whereUsername($author)->orWhere('id', intval($author))->firstOrFail();
    }
}