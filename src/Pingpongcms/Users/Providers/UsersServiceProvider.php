<?php

namespace Pingpongcms\Users\Providers;

use Form;
use Illuminate\Support\ServiceProvider;
use Pingpongcms\Users\Http\Composers\SharePermissions;
use Pingpongcms\Users\Http\Composers\ShareRoles;
use Pingpongcms\Users\Role;

class UsersServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../../migrations' => base_path('database/migrations')
        ], 'migrations');

        $this->loadTranslationsFrom(__DIR__.'/../../../resources/lang', 'users');

        $this->registerViews();

        $this->registerViewComposers();

        $this->addSettingTabs();
    }

    protected function registerViews()
    {
        $source = __DIR__.'/../../../resources/views';
        $destination = base_path('resources/views/vendor/pingpongcms/users');

        $this->publishes([$source => $destination], 'views');

        $this->loadViewsFrom([$destination, $source], 'users');
    }

    private function addSettingTabs()
    {
        $this->app['settings.tabs']->add('Users', function ($tab) {
            $tab->addSetting('Default Role:', 'user_default_role', function ()
            {
                $roles = Role::pluck('description', 'id')->toArray();

                return Form::select('settings[user_default_role]', $roles, setting('user_default_role'), ['class' => 'form-control']);
            }, false);
        })->order(2);
    }

    public function registerViewComposers()
    {
        $this->app['view']->composer('users::admin.form', ShareRoles::class);
        $this->app['view']->composer('users::roles.form', SharePermissions::class);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
