<?php

namespace Pingpongcms\Users\Traits;

use Pingpongcms\Users\Permission;
use Pingpongcms\Users\Role;

trait HasRoles
{
    /**
     * A user may have multiple roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Assign the given role to the user.
     *
     * @param string $role
     *
     * @return mixed
     */
    public function assignRole($name)
    {
        return $this->roles()->save(
            Role::whereName($name)->orWhere('id', $name)->firstOrFail()
        );
    }

    public function addRole($name)
    {
        return $this->assignRole($name);
    }

    /**
     * Determine if the user has the given role.
     *
     * @param mixed $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param \App\Permission $permission
     *
     * @return bool
     */
    public function hasPermission(Permission $permission)
    {
        return $this->hasRole($permission->roles);
    }

    /**
     * Accessor for "role_list" attribute.
     * 
     * @return array
     */
    public function getRoleListAttribute()
    {
        return $this->roles->lists('id', 'name')->toArray();
    }

    /**
     * Determine whether the user is have the given role(s).
     * 
     * @param  string|array  $role
     * @return boolean
     */
    public function is($role)
    {
        if (func_num_args() > 1) {
            $roles = func_get_args();
        } else {
            $roles = is_array($role) ? $role : (array) $role;
        }

        foreach ($roles as $name) {
            if ($this->hasRole($name)) {
                return true;
            }
        }

        return false;
    }
}
